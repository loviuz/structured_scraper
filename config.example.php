<?php

// URL da cui iniziare lo scraping
$start_urls = [
    'https://www.iltuocomune.it/amministrazione-trasparente-url'
];

// Regole di estrazione link.
// Ogni regola è applicata alle pagine trovate in base al livello di profondità.
// Esempio: la prima regola viene applicata alle start_urls, la seconda alle
// sotto-pagine, e così via.
$link_rules = [
    '//td[@class="actions"]/a[@title="Visualizza "]',
    '//table[@class="allegati"]/a[@title="Download versione non firmata"]'
];

// Mimetype da salvare, con associazione mime-type => estensione con cui salvare
// i file
$allowedMimetypes = [
    'application/pdf' => 'pdf'
];

// Specifica se visualizzare le URL scansionate
$debug = true;

// Directory dove salvare i file trovati
$download_dir = __DIR__.'/pdf';
