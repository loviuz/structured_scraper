# Esempio

Copia questa cartella nel tuo web server in modo che sia raggiungibile ad esempio a questa URL:

`
http://localhost/example
`

Copia anche il file `example/config.php` nella root dello scraper, così da configurare l'URL e come filtri di ricerca ad ogni livello di profondità un semplice tag `<a>` per il test.

Inserendo negli `$start_url` l'indirizzo sopra e avviando lo script, dovreste trovare nella cartella `pdf/` il file `test.pdf` e l'output dovrebbe essere il seguente:

```
[0] http://localhost/example
[1] http://localhost/example/pagina1
[1] http://localhost/example/pagina2
[2] http://localhost/example/pagina1/sub1.1
[2] http://localhost/example/pagina1/sub1.2
[2] http://localhost/example/pagina1/sub1.3
[2] http://localhost/example/pagina2/sub2.1
[2] http://localhost/example/pagina2/sub2.2
[2] http://localhost/example/pagina2/sub2.3
[3] http://localhost/example/pagina1/sub1.1/subsub1.1.1
[3] http://localhost/example/pagina1/sub1.1/subsub1.1.2
[3] http://localhost/example/pagina1/sub1.1/test.pdf
[*] Downloading test.pdf
```