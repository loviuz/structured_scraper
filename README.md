# Structured Scraper
Uno scraper di documenti configurabile e strutturato.

Permette di definire una lista di URL da scansionare e per ciascuna viene letto il contenuto HTML, vengono estratti eventuali sub-link per scansionare le pagine più interne, fino a che non si arriva a un link con mimetype definito nel file di configurazione per scaricarlo.

Qual è la novità? Che non scansiona tutti i link ma solo quelli corrispondenti ad un filtro XPath configurabile per ogni livello di sub-link, per cui dal primo URL potrebbe scansionare tutti i link dentro un `<div>` con classe `.actions`, dal secondo URL tutti i link dentro un `<td>` dentro una `table` con un determinato `id` e così via.

In questo modo lo scraping va configurato su siti web che hanno una determinata struttura per arrivare al documenti in modo preciso e rapido.

## Come installare
Scaricare le dipendende con composer:

`
php composer.phar install
`

## Come funziona
Occorre creare un file di configurazione partendo dal file di esempio `config.example.php`. In questo file vanno definiti:
- **$start_url:** è un array contenente le URL di partenza da scansionare
- **$link_rules:** sono le regole per trovare i sub-link. Partendo dalla prima URL infatti lo script non scansiona tutti i link ma solamente quelli che corrispondono al pattern XPath definito in questo array, in ordine di profondità, cioè nel primo URL scansionato vengono filtrati i sub-link in base alla regola XPath con indice 0, nella pagina contenuta nel prossimo sub-link verranno estratti i link in base alla regola XPath con indice 1, e così via
- **$allowedMimetypes:** è un array che indica quali file scaricare in base al mime-type. E' un array associativo perché quando viene trovato un link con il mime-type corrispondente viene scaricato applicando l'estensione associata (nei casi in cui il nome del file non sia chiaro dal'URL)
- **$download_dir:** è semplicemente la directory dove salvare i file trovati.

## Demo
Per provarlo in funzione è possibile usare la cartella `example` come test. All'interno ci sono le [istruzioni](example/).

## TODO
- [ ] Salvare in un database locale le URL già scansionate, per evitare di ri-scaricare gli stessi file
- [ ] Eseguire le chiamate http in parallelo per velocizzare la procedura